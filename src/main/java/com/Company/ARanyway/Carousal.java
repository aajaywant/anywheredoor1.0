package com.Company.ARanyway;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;

public class Carousal extends AppCompatActivity {

    ViewFlipper imgFlipper;
    //Button skip;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_carousal);
        //skip=(Button) findViewById(R.id.skip);

        Button button=(Button) findViewById(R.id.button);
        button.setVisibility(View.INVISIBLE);
        button.postDelayed(new Runnable() {
            @Override
            public void run() {
                button.setVisibility(View.VISIBLE);
            }
        },15000);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Carousal.this,MainActivity.class);
                startActivity(intent);
            }
        });

        int sliders[]={
            R.drawable.arsenal,
            R.drawable.beach,
                R.drawable.taj,
                R.drawable.eiffel,
                R.drawable.ice,
                R.drawable.city


        };

        imgFlipper=findViewById(R.id.imgFlipper);

        for(int slide:sliders){
            sliderFlipper(slide);
        }
        /*

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Carousal.this,ButtonLayout.class);
                startActivity(intent);
            }
        });

         */



    }
    public void sliderFlipper(int image){
        ImageView imageView=new ImageView(this);
        imageView.setBackgroundResource(image);

        imgFlipper.addView(imageView);
        imgFlipper.setFlipInterval(4000);
        imgFlipper.setAutoStart(true);
        imgFlipper.setInAnimation(this,android.R.anim.fade_in);
        imgFlipper.setOutAnimation(this,android.R.anim.fade_out);


    }
}
